﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabLoader : MonoBehaviour
{
	/* Public Variables */
	//public PlayerController player;
	//public CameraController cameraController;
	public PauseMenu pauseMenu;
	public AudioManager audioManager;
	public GameManager gameManager;

	void Awake()
	{
		/*if (!PlayerController.instance)
		{
			Instantiate(player);
		}

		if (!CameraController.instance)
		{
			Instantiate(cameraController);
		}*/

		if (!PauseMenu.instance)
		{
			Instantiate(pauseMenu);
		}

		if (!AudioManager.instance)
		{
			Instantiate(audioManager);
		}

		if (!GameManager.instance)
		{
			Instantiate(gameManager);
		}
	}

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}
}
