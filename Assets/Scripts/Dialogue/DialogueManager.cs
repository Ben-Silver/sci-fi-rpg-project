﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
	/* Public Variables */
	public Text dialogueText;
	public Text dialogueName;
	public GameObject dialogueBox;
	public GameObject nameBox;
	public int currentLine;
	public string[] dialogueLines;

	/* Private Variables */
	public bool startedDialogue;

	#region Singleton
	public static DialogueManager instance;

	void Awake()
	{
		// Check to see if there is already an instance of the gameObject in the scene
		if (instance == null)
		{
			// Set the instance to this gameObject
			instance = this;
		}
		else
		{
			// Remove unwanted instances
			Destroy(gameObject);
		}

		// Make sure the gameObject isn't destroyed when a new level is loaded
		DontDestroyOnLoad(gameObject);
	}
	#endregion

	// Use this for initialization
	void Start()
	{
		dialogueText.text = dialogueLines[currentLine];
	}
	
	// Update is called once per frame
	void Update()
	{
		// Update the dialogue
		UpdateDialogue();
	}

	///<Summary>
	/// Function to update the dialogue text
	///</Summary>
	void UpdateDialogue()
	{
		// Check to see if the box is active and the current line isn't exceeding the maximum dialogue length
		if (dialogueBox.activeInHierarchy && currentLine < (dialogueLines.Length))
		{
			// Check to see if the mouse is being clicked
			if (Input.GetButtonUp("Fire1"))
			{
				// Check to see if the dialogue has just started (prevents skipping)
				if (!startedDialogue)
				{
					// Increment the current line is the mouse is being clicked
					currentLine += 1;
					
					// Make sure the current line doesn't exceed the bounds of the array
					if (currentLine >= dialogueLines.Length)
					{
						// De-activate the dialogue box if all the dialouge has been displayed
						dialogueBox.SetActive(false);

						// Allow the player to move again
						GameManager.instance.dialogueActive = false;

						// Reset currentLine
						currentLine = 0;
						dialogueLines = null;
					}
					else
					{
						// Check to see if the line being displayed has a name tag
						CheckName();

						// Jump to the correct line of dialogue
						dialogueText.text = dialogueLines[currentLine];
					}
				}
				else
				{
					// Turn off startedDialogue so the dialogue can progress
					startedDialogue = false;
				}
			}
		}
	}

	///<Summary>
	/// Function to display the dialogue text
	///</Summary>
	/// <param name="dialogue">An array of dialogue to be displayed on screen</param>
	/// <param name="showName">Determines whether the name is to be displayed</param>
	public void ShowDialogue(string[] dialogue, bool showName)
	{
		// Stop the player from moving
		GameManager.instance.dialogueActive = true;

		// Set the dialogueLines to what has been passed in
		dialogueLines = dialogue;

		// Reset currentLine
		currentLine = 0;

		// Check to see if the line being displayed has a name tag
		CheckName();

		// Set the current line of text to the first line of dialogueLines
		dialogueText.text = dialogueLines[currentLine];

		// Show the dialogueBox
		dialogueBox.SetActive(true);

		// Set startedDialogue to true
		startedDialogue = true;

		// Show/hide the name box depending on the value of showName
		nameBox.SetActive(showName);
	}

	///<Summary>
	/// Function to check if currentLine has a name tag
	///</Summary>
	public void CheckName()
	{
		// Check to see if the current line of dialogue starts with the name tag
		if (dialogueLines[currentLine].StartsWith("n-"))
		{
			// Display that line in the name box without the name tag
			dialogueName.text = dialogueLines[currentLine].Replace("n-", "");

			// Move to the next line
			currentLine += 1;
		}
	}
}
