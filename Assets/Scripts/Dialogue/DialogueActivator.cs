﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueActivator : MonoBehaviour
{
	/* Public Variables */
	public bool showName;
	public string[] dialogueLines;
	
	/* Private Variables */
	private bool canActivate;

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		// Check to see if canActivate is true and the mouse is being pressed
		if (canActivate && Input.GetButtonDown("Fire1") && !DialogueManager.instance.dialogueBox.activeInHierarchy)
		{
			// Show the dialogue box
			DialogueManager.instance.ShowDialogue(dialogueLines, showName);
		}
	}

	// When an object enters the proximity of the gameObject
	private void OnTriggerEnter2D(Collider2D other)
	{
		// Set canActivate based on whether the object has the "Player" tag
		canActivate = (other.gameObject.tag == "Player") ? true : false;
	}

	// When an object leaves the proximity of the gameObject
	private void OnTriggerExit2D(Collider2D other)
	{
		// Set canActivate based on whether the object has the "Player" tag
		canActivate = (other.gameObject.tag == "Player") ? false : true;
	}
}
