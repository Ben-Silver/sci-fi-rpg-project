﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	/* Public Variables */
	[Header("Character's Stats")]
	public CharacterStats[] characterStats;

	[Header("Action Variables")]
	public bool fadingToLevel;
	public bool dialogueActive;
	public bool menuOpen;
	public bool shopOpen;
	public bool battleActive;
	public bool gameIsBusy;
	public bool loadingGame;

	[Header("Inventory Components")]
	public int[] itemQuantity;
	public List<Item> itemInventory;

	[Header("Gold")]
	public int currentGold;

	[Header("Game Event Flags")]
	public List<GameSwitch> gameSwitches;

	/* Private Variables */
	private int startingGold = 3000;

	#region Singleton
	public static GameManager instance;

	void Awake()
	{
		// Check to see if there is already an instance of the gameObject in the scene
		if (instance == null)
		{
			// Set the instance to this gameObject
			instance = this;
		}
		else
		{
			// Remove unwanted instances
			Destroy(gameObject);
		}

		// Make sure the gameObject isn't destroyed when a new level is loaded
		DontDestroyOnLoad(gameObject);
	}
	#endregion

	// Use this for initialization
	void Start() 
	{
		// Find all instances of CharacterStats objects
		//characterStats = FindObjectsOfType<CharacterStats>();
		currentGold = startingGold;
	}
	
	// Update is called once per frame
	void Update()
	{
		if (fadingToLevel || dialogueActive || menuOpen || shopOpen || battleActive)
		{
			PlayerController.instance.playerCanMove = false;
			gameIsBusy = true;
		}
		else
		{
			PlayerController.instance.playerCanMove = true;
			gameIsBusy = false;
		}
	}

	/// <summary>
	/// Function to fetch the details of an item
	/// </summary>
	public Item GetItemDetails(string itemName)
	{
		// Loop through the array
		for (int i = 0; i < itemInventory.Count; i++)
		{
			// Compare the name of the reference item at i to the name of the item passed in
			if (itemInventory[i].itemName == itemName)
			{
				// Return the specified item
				return itemInventory[i];
			}
		}

		return null;
	}

	/// <summary>
	/// Function to sort the items in the array
	/// </summary>
	public void SortItems()
	{
		// 
		bool itemAfterSpace = true;

		// Loop while there are spaces between items
		while (itemAfterSpace)
		{
			// Set itemAfterSpace to be false
			itemAfterSpace = false;

			// Loop through the array
			for (int i = 0; i < itemQuantity.Length - 1; i++)
			{
				// If the item quantity at index i is empty
				if (itemQuantity[i] == 0)
				{
					// Set the item in the next index to the current index
					itemQuantity[i] = itemQuantity[i + 1];

					// Remove the item at the next index
					itemQuantity[i + 1] = 0;

					// If the item at index i is not empty
					if (itemQuantity[i] != 0)
					{
						// Set itemAfterSpace to be true
						itemAfterSpace = true;
					}
				}
			}
		}
	}

	/// <summary>
	/// Function to add the item in the chest to the inventory
	/// </summary>
	public void AddItem(Item itemToAdd)
	{
		// If the list isn't empty
		if (itemInventory.Count != 0)
		{
			// Loop through the list
			for (int i = 0; i < itemInventory.Count; i++)
			{
				// If the inventory doesn't contain the item
				if (!itemInventory.Contains(itemToAdd))
				{
					// Add the item
					itemInventory.Add(itemToAdd);

					// Set the quantity at the new index
					itemQuantity[itemInventory.IndexOf(itemToAdd)] += 1;

					// Return out of the function
					return;
				}

				// If the item is already in the inventory
				if (itemInventory[i].itemName == itemToAdd.itemName)
				{
					// Increase the item quantity by 1
					itemQuantity[i] += 1;

					// Return out of the function
					return;
				}
			}
		}
		else
		{
			// Add the item
			itemInventory.Add(itemToAdd);

			// Set the quantity at the new index
			itemQuantity[itemInventory.IndexOf(itemToAdd)] += 1;
		}
	}

	/// <summary>
	/// Function to remove an item from the inventory
	/// </summary>
	public void RemoveItem(Item itemToRemove)
	{
		if (itemInventory.Contains(itemToRemove))
		{
			itemQuantity[itemInventory.IndexOf(itemToRemove)] -= 1;

			if (itemQuantity[itemInventory.IndexOf(itemToRemove)] == 0)
			{
				itemInventory.Remove(itemToRemove);
			}
		}

		SortItems();
		PauseMenu.instance.ShowItemSlots();
	}

	/// <summary>
	/// Function to get the current gold the player has
	/// </summary>
	public int GetGold()
	{
		// Get the currentGold and set it into a local variable
		int gold = currentGold;

		// Return the local variable
		return gold;
	}

	/// <summary>
	/// Function to add gold to the player's currentGold
	/// </summary>
	/// <param name="newGold">The amount being added</param>
	public void AddGold(int newGold)
	{
		// Add the amount passed in to the currentGold
		currentGold += newGold;
	}

	/// <summary>
	/// Function to save the game data
	/// </summary>
	public void SaveData()
	{
		// Save the map the player is on
		PlayerPrefs.SetString("Current_Scene", SceneManager.GetActiveScene().name);

		// Save the player's X, Y and Z position
		PlayerPrefs.SetFloat("Player_Position_X", PlayerController.instance.transform.position.x);
		PlayerPrefs.SetFloat("Player_Position_Y", PlayerController.instance.transform.position.y);
		PlayerPrefs.SetFloat("Player_Position_Z", PlayerController.instance.transform.position.z);

		// Save the player's direction
		PlayerPrefs.SetFloat("Player_Direction_X", PlayerController.instance.lastMoveDir.x);
		PlayerPrefs.SetFloat("Player_Direction_Y", PlayerController.instance.lastMoveDir.y);

		// Loop through the character stats
		for (int i = 0; i < characterStats.Length; i++)
		{
			// Make sure the character is active in the hierarchy
			if (characterStats[i].gameObject.activeInHierarchy)
			{
				// Save the character as active
				PlayerPrefs.SetInt("Player_" + characterStats[i].charName + "_active", 1);
			}
			else
			{
				// Save the character as inactive
				PlayerPrefs.SetInt("Player_" + characterStats[i].charName + "_active", 0);
			}

			// Save each of the character's stats
			PlayerPrefs.SetString("Player_" + characterStats[i].charName + "_name", characterStats[i].charName);
			PlayerPrefs.SetInt("Player_" + characterStats[i].charName + "_current_level", characterStats[i].currentLevel);
			PlayerPrefs.SetInt("Player_" + characterStats[i].charName + "_maximum_level", characterStats[i].maximumLevel);
			PlayerPrefs.SetInt("Player_" + characterStats[i].charName + "_current_EXP", characterStats[i].currentEXP);
			PlayerPrefs.SetInt("Player_" + characterStats[i].charName + "_base_EXP", characterStats[i].baseEXP);
			PlayerPrefs.SetInt("Player_" + characterStats[i].charName + "_current_HP", characterStats[i].currentHP);
			PlayerPrefs.SetInt("Player_" + characterStats[i].charName + "_maximum_HP", characterStats[i].maximumHP);
			PlayerPrefs.SetInt("Player_" + characterStats[i].charName + "_current_MP", characterStats[i].currentMP);
			PlayerPrefs.SetInt("Player_" + characterStats[i].charName + "_maximum_MP", characterStats[i].maximumMP);
			PlayerPrefs.SetInt("Player_" + characterStats[i].charName + "_pAttack", characterStats[i].pAttack);
			PlayerPrefs.SetInt("Player_" + characterStats[i].charName + "_pDefence", characterStats[i].pDefence);
			PlayerPrefs.SetInt("Player_" + characterStats[i].charName + "_mAttack", characterStats[i].mAttack);
			PlayerPrefs.SetInt("Player_" + characterStats[i].charName + "_mDefence", characterStats[i].mDefence);
			PlayerPrefs.SetInt("Player_" + characterStats[i].charName + "_speed", characterStats[i].speed);
			PlayerPrefs.SetInt("Player_" + characterStats[i].charName + "_weapon_power", characterStats[i].weaponPower);
			PlayerPrefs.SetInt("Player_" + characterStats[i].charName + "_weapon_magic", characterStats[i].weaponMagic);
			PlayerPrefs.SetInt("Player_" + characterStats[i].charName + "_armour_power", characterStats[i].armourPower);
			PlayerPrefs.SetInt("Player_" + characterStats[i].charName + "_armour_magic", characterStats[i].armourMagic);

			// If the player is holding a weapon
			if (characterStats[i].equippedWeapon != null)
			{
				// Save the name of the equipped weapon
				PlayerPrefs.SetString("Player_" + characterStats[i].charName + "_equipped_weapon", characterStats[i].equippedWeapon.itemName);
			}
			else
			{
				// Save the weapon name as "None"
				PlayerPrefs.SetString("Player_" + characterStats[i].charName + "_equipped_weapon", "None");
			}

			// If the player is wearing armour
			if (characterStats[i].equippedArmour != null)
			{
				// Save the name of the equipped armour
				PlayerPrefs.SetString("Player_" + characterStats[i].charName + "_equipped_armour", characterStats[i].equippedArmour.itemName);
			}
			else
			{
				// Save the armour name as "None"
				PlayerPrefs.SetString("Player_" + characterStats[i].charName + "_equipped_armour", "None");
			}

			/*public Sprite charSprite;
			public int[] expToNextLevel;
			public int[] mpLevelBonus;*/
		}

		// Loop through the inventory list
		for (int i = 0; i < itemInventory.Count; i++)
		{
			// Save the name of the item in each slot
			PlayerPrefs.SetString("Item_Slot_" + i, itemInventory[i].itemName);
			
			// Save the number of items held in that slot
			PlayerPrefs.SetInt("Item_Slot_Quant" + i, itemQuantity[i]);
		}

		// Store the total number of slots occupied by a single item (for loading purposes)
		PlayerPrefs.SetInt("Total_Items", itemInventory.Count);


	}

	/// <summary>
	/// Function to load the game data
	/// </summary>
	public void LoadData()
	{
		// Set the player's position on the map
		PlayerController.instance.transform.position = new Vector3(PlayerPrefs.GetFloat("Player_Position_X"),
																   PlayerPrefs.GetFloat("Player_Position_Y"),
																   PlayerPrefs.GetFloat("Player_Position_Z"));

		// Set the player's direction on the map
		PlayerController.instance.lastMoveDir = new Vector2(PlayerPrefs.GetFloat("Player_Direction_X"),	PlayerPrefs.GetFloat("Player_Direction_Y"));

		// Character info
		for (int i = 0; i < characterStats.Length; i++)
		{
			// Load each of the character's stats
			characterStats[i].charName = PlayerPrefs.GetString("Player_" + characterStats[i].charName + "_name");
			characterStats[i].currentLevel = PlayerPrefs.GetInt("Player_" + characterStats[i].charName + "_current_level");
			characterStats[i].maximumLevel = PlayerPrefs.GetInt("Player_" + characterStats[i].charName + "_maximum_level");
			characterStats[i].currentEXP = PlayerPrefs.GetInt("Player_" + characterStats[i].charName + "_current_EXP");
			characterStats[i].baseEXP = PlayerPrefs.GetInt("Player_" + characterStats[i].charName + "_base_EXP");
			characterStats[i].currentHP = PlayerPrefs.GetInt("Player_" + characterStats[i].charName + "_current_HP");
			characterStats[i].maximumHP = PlayerPrefs.GetInt("Player_" + characterStats[i].charName + "_maximum_HP");
			characterStats[i].currentMP = PlayerPrefs.GetInt("Player_" + characterStats[i].charName + "_current_MP");
			characterStats[i].maximumMP = PlayerPrefs.GetInt("Player_" + characterStats[i].charName + "_maximum_MP");
			characterStats[i].pAttack = PlayerPrefs.GetInt("Player_" + characterStats[i].charName + "_pAttack");
			characterStats[i].pDefence = PlayerPrefs.GetInt("Player_" + characterStats[i].charName + "_pDefence");
			characterStats[i].mAttack = PlayerPrefs.GetInt("Player_" + characterStats[i].charName + "_mAttack");
			characterStats[i].mDefence = PlayerPrefs.GetInt("Player_" + characterStats[i].charName + "_mDefence");
			characterStats[i].speed = PlayerPrefs.GetInt("Player_" + characterStats[i].charName + "_speed");
			characterStats[i].weaponPower = PlayerPrefs.GetInt("Player_" + characterStats[i].charName + "_weapon_power");
			characterStats[i].weaponMagic = PlayerPrefs.GetInt("Player_" + characterStats[i].charName + "_weapon_magic");
			characterStats[i].armourPower = PlayerPrefs.GetInt("Player_" + characterStats[i].charName + "_armour_power");
			characterStats[i].armourMagic = PlayerPrefs.GetInt("Player_" + characterStats[i].charName + "_armour_magic");

			// Check to see if the character was holding a weapon
			if (PlayerPrefs.GetString("Player_" + characterStats[i].charName + "_equipped_weapon") == "None")
			{
				// Set the equipped weapon to null
				characterStats[i].equippedWeapon = null;
			}
			else
			{
				// Find the weapon file by name and equip it to the character
				characterStats[i].equippedWeapon = Resources.Load<Item>("Item Files/" + PlayerPrefs.GetString("Player_" + characterStats[i].charName + "_equipped_weapon"));
			}

			// Check to see if the character was wearing armour
			if (PlayerPrefs.GetString("Player_" + characterStats[i].charName + "_equipped_armour") == "None")
			{
				// Set the equipped armour to null
				characterStats[i].equippedArmour = null;
			}
			else
			{
				// Find the armour file by name and equip it to the character
				characterStats[i].equippedArmour = Resources.Load<Item>("Item Files/" + PlayerPrefs.GetString("Player_" + characterStats[i].charName + "_equipped_armour"));
			}

			// See if the character was active in the hierarchy
			if (PlayerPrefs.GetInt("Player_" + characterStats[i].charName + "_active") == 1)
			{
				// Set the player to be active
				characterStats[i].gameObject.SetActive(true);
			}
			else
			{
				// Set the player to be inactive
				characterStats[i].gameObject.SetActive(false);
			}
		}

		// Loop until the number of items in the inventory is reached
		for (int i = 0; i < PlayerPrefs.GetInt("Total_Items"); i++)
		{
			// Find the item file by name and add it to the inventory
			itemInventory.Add(Resources.Load<Item>("Item Files/" + PlayerPrefs.GetString("Item_Slot_" + i)));

			// Load the item quantity in at the associated index
			itemQuantity[i] = PlayerPrefs.GetInt("Item_Slot_Quant" + i);
		}

		// Set loadingGame to true
		loadingGame = true;
	}
}
