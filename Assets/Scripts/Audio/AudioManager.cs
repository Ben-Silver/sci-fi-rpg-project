﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
	/* Public Variables */
	public AudioSource[] BGM;
	public AudioSource[] SFX;

	#region Singleton
	public static AudioManager instance;

	void Awake()
	{
		// Check to see if there is already an instance of the gameObject in the scene
		if (instance == null)
		{
			// Set the instance to this gameObject
			instance = this;
		}
		else
		{
			// Remove unwanted instances
			Destroy(gameObject);
		}

		// Make sure the gameObject isn't destroyed when a new level is loaded
		DontDestroyOnLoad(gameObject);
	}
	#endregion

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}

	/// <summary>
	/// Function to play BGM by name
	/// </summary>
	/// <param name="bgmName">The name of the BGM in the array</param>
	public void PlayBGM(string bgmName)
	{
		// Loop through the array
		for (int i = 0; i < SFX.Length; i++)
		{
			// Compare the name of each track with the string passed in
			if (BGM[i].gameObject.name == bgmName)
			{
				if (BGM[i].isPlaying)
				{
					// Stop playing the current audio track
					StopBGM();

					// Play the corresponding track
					BGM[i].Play();

					// Return out of the function
					return;
				}
			}
		}

		Debug.Log("Audio File not found");
	}

	/// <summary>
	/// Function to play BGM by index
	/// </summary>
	/// <param name="bgmIndex">The index of the BGM in the array</param>
	public void PlayBGM(int bgmIndex)
	{
		// Make sure the music to play next isn't already playing
		if (!BGM[bgmIndex].isPlaying)
		{
			// Stop playing the current audio track
			StopBGM();
		
			// Make sure the passed value is within the array bounds
			if (bgmIndex < BGM.Length)
			{
				// Play the audio file at bgmIndex
				BGM[bgmIndex].Play();
			}
		}
	}

	/// <summary>
	/// Function to play SFX by name
	/// </summary>
	/// <param name="sfxName">The name of the SFX in the array</param>
	public void PlayeSFX(string sfxName)
	{
		// Loop through the array
		for (int i = 0; i < SFX.Length; i++)
		{
			// Compare the name of each track with the string passed in
			if (SFX[i].gameObject.name == sfxName)
			{
				// Play the corresponding track
				SFX[i].Play();

				// Return out of the function
				return;
			}
		}

		Debug.Log("Audio File not found");
	}

	/// <summary>
	/// Function to play SFX by index
	/// </summary>
	/// <param name="sfxIndex">The index of the SFX in the array</param>
	public void PlayeSFX(int sfxIndex)
	{
		// Make sure the passed value is within the array bounds
		if (sfxIndex < SFX.Length)
		{
			// Play the audio file at sfxIndex
			SFX[sfxIndex].Play();
		}
	}

	/// <summary>
	/// Function to stop playing the current BGM
	/// </summary>
	public void StopBGM()
	{
		// Loop through the array
		for (int i = 0; i < BGM.Length; i++)
		{
			// Stop the audio at position i
			BGM[i].Stop();
		}
	}
}
