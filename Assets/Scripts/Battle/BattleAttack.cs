﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BattleAttack
{
	/* Public Variables */
	public string attackName;
	public AttackType attackType;
	public int movePower;
	public int moveCost;
	public BattleAnimation battleAnimation;
}

public enum AttackType
{
	Physical,
	Magical,
	Status,
	Stat
}