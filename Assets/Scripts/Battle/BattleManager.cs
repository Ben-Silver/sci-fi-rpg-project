﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleManager : MonoBehaviour
{
	/* Public Variables */
	[Header("Turns")]
	public TurnStates turnStates;
	public int currentTurn;
	public int turnCounter;

	[Header("UI Elements")]
	public GameObject battleScene;
	public GameObject actionButtonsWindow;
	public GameObject targetWindow;
	public GameObject magicWindow;
	public BattleMessage battleMessage;
	public Text[] playerName;
	public Text[] playerHP;
	public Text[] playerMP;
	public BattleCharacter[] players;
	public BattleCharacter[] enemies;
	public Transform[] playerPositions;
	public Transform[] enemyPositions;
	public List<BattleCharacter> activeCharacters;
	public TargetButton[] targetButtons;
	public MagicButton[] magicButtons;

	[Header("Attacks List")]
	public BattleAttack[] battleAttacks;
	public GameObject enemyAttackEffect;
	public DamageNumber damageNumber;

	/* Private Variables */
	private bool battleActive;

	#region Singleton
	public static BattleManager instance;

	void Awake()
	{
		// Check to see if there is already an instance of the gameObject in the scene
		if (instance == null)
		{
			// Set the instance to this gameObject
			instance = this;
		}
		else
		{
			// Remove unwanted instances
			Destroy(gameObject);
		}

		// Make sure the gameObject isn't destroyed when a new level is loaded
		DontDestroyOnLoad(gameObject);
	}
	#endregion

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		// If the battle is active
		if (battleActive)
		{
			// Player's turn
			if (turnStates == TurnStates.Idle)
			{
				// If the current turn character is a player
				if (activeCharacters[currentTurn].characterType == CharacterType.Player)
				{
					// Show the action buttons window
					actionButtonsWindow.SetActive(true);
				}
				else
				{
					// Hide the action buttons window
					actionButtonsWindow.SetActive(false);

					// Enemy attack
					StartCoroutine(EnemyMove());
				}
			}
			
			// Detect input
			if (Input.GetKeyDown(KeyCode.N))
			{
				// Change the turn
				ChangeTurns();
			}
		}

		// Update the UI Stats
		UpdateUIStats();
	}

	/// <summary>
	/// Function to start a battle
	/// </summary>
	/// <param name="bEnemies">The enemies being fought</param>
	public void BattleStart(string[] bEnemies)
	{
		// Make sure there isn't already a battle taking place
		if (!battleActive)
		{
			// Set battleActive to true
			battleActive = true;

			// Stop the player from moving
			GameManager.instance.battleActive = true;

			// Ensure that the battle scene is always aligned with the camera
			transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, 0);

			// Show the battle scene
			battleScene.SetActive(true);

			// Play the battle music
			AudioManager.instance.PlayBGM(0);

			// Loop through the player array
			for (int i = 0; i < playerPositions.Length; i++)
			{
				// Make sure the characterStats object at i is active
				if (GameManager.instance.characterStats[i].gameObject.activeInHierarchy)
				{
					// Loop through the array
					for (int j = 0; j < players.Length; j++)
					{
						// Ensure the player at j has the same name as the player at i
						if (players[j].characterName == GameManager.instance.characterStats[i].charName)
						{
							// Make a new instance of the player
							BattleCharacter newCharacter = Instantiate(players[j], playerPositions[i].position, playerPositions[i].rotation);

							// Parent the new instance to the corresponding position
							newCharacter.transform.parent = playerPositions[i];

							// Add the new character to the activeCharacters list
							activeCharacters.Add(newCharacter);
							
							// Create a new CharacterStats object
							CharacterStats newCharacterStats = GameManager.instance.characterStats[i];
							
							// Set the stats of the new character
							activeCharacters[i].currentHP = newCharacterStats.currentHP;
							activeCharacters[i].maximumHP = newCharacterStats.maximumHP;
							activeCharacters[i].currentMP = newCharacterStats.currentMP;
							activeCharacters[i].maximumMP = newCharacterStats.maximumMP;
							activeCharacters[i].currentLevel = newCharacterStats.currentLevel;
							activeCharacters[i].pAttack = newCharacterStats.pAttack;
							activeCharacters[i].pDefence = newCharacterStats.pDefence;
							activeCharacters[i].mAttack = newCharacterStats.mAttack;
							activeCharacters[i].mDefence = newCharacterStats.mDefence;
							activeCharacters[i].speed = newCharacterStats.speed;
							activeCharacters[i].weaponPower = newCharacterStats.weaponPower;
							activeCharacters[i].weaponMagic = newCharacterStats.weaponMagic;
							activeCharacters[i].armourPower = newCharacterStats.armourPower;
							activeCharacters[i].armourMagic = newCharacterStats.armourMagic;
						}
					}
				}
			}

			// Loop through the enemy array
			for (int i = 0; i < bEnemies.Length; i++)
			{
				// Make sure the enemy at i exists
				if (bEnemies[i] != null)
				{
					// Loop through the array
					for (int j = 0; j < enemies.Length; j++)
					{
						// Ensure the player at j has the same name as the player at i
						if (enemies[j].characterName == bEnemies[i])
						{
							// Make a new instance of the enemy
							BattleCharacter newEnemy = Instantiate(enemies[j], enemyPositions[i].position, enemyPositions[i].rotation);

							// Parent the new instance to the corresponding position
							newEnemy.transform.parent = enemyPositions[i];

							// Add the new enemy to the list of active characters
							activeCharacters.Add(newEnemy);
						}
					}
				}
			}
		}
	}

	/// <summary>
	/// Function to change who's turn it is
	/// </summary>
	public void ChangeTurns()
	{
		// Set the turn state to active
		turnStates = TurnStates.Idle;

		// Make sure the currentTurn is within the list bounds
		if (currentTurn < activeCharacters.Count)
		{
			// Increment currentTurn
			currentTurn++;

			// Increment turnCounter
			turnCounter++;
		}

		// If currentTurn is at the end of the list
		if (currentTurn == activeCharacters.Count)
		{
			// Reset currentTurn
			currentTurn = 0;
		}

		// Update the battle state
		UpdateBattle();
	}

	/// <summary>
	/// Function to update the battle status
	/// </summary>
	public void UpdateBattle()
	{
		// Local variables
		bool playersDead = true;
		bool enemiesDead = true;

		// Loop through the active characters array
		for (int i = 0; i < activeCharacters.Count; i++)
		{
			// Clamp the HP to make sure it isn't negative
			if (activeCharacters[i].currentHP < 1)
			{
				activeCharacters[i].currentHP = 0;
			}

			// If the HP is 0
			if (activeCharacters[i].currentHP == 0)
			{
				// Handle dead battler
			}
			else
			{
				// If the character at index i is a player
				if (activeCharacters[i].characterType == CharacterType.Player)
				{
					// Set players dead to false
					playersDead = false;
				}
				// If the character at index i is an enemy
				else if (activeCharacters[i].characterType == CharacterType.Enemy)
				{
					// Set enemies dead to false
					enemiesDead = false;
				}
			}
		}

		// If all players or enemies are defeated
		if (playersDead || enemiesDead)
		{
			// For all enemies
			if (enemiesDead)
			{
				// Victory
			}
			// For all players
			else if (playersDead)
			{
				// Defeat
			}

			// Hide the battle scene
			battleScene.SetActive(false);

			// Set battleActive to false
			battleActive = false;

			// Allow the player to move again
			GameManager.instance.battleActive = false;
		}
		else
		{
			// While the character whose turn it is is dead
			while (activeCharacters[currentTurn].currentHP == 0)
			{
				// Skip the dead player's turn
				currentTurn++;

				// Check to see if currentTurn is at its maximum
				if (currentTurn >= activeCharacters.Count)
				{
					// Reset currentTurn
					currentTurn = 0;
				}
			}
		}
	}

	/// <summary>
	/// Function to automate the enemy turn
	/// </summary>
	public IEnumerator EnemyMove()
	{
		// Set the turn state to active
		turnStates = TurnStates.Active;

		// Wait for 1 second
		yield return new WaitForSeconds(1);

		// Make the enemy take their turn
		EnemyTurn();

		// Wait for 1 second
		yield return new WaitForSeconds(1);

		// Move to the next turn
		ChangeTurns();
	}

	/// <summary>
	/// Function to make the enemy take their turn
	/// </summary>
	public void EnemyTurn()
	{
		// Create a new list
		List<int> players = new List<int>();

		// Loop through the active battlers list
		for (int i = 0; i < activeCharacters.Count; i++)
		{
			// Check the character type of the character at i
			if (activeCharacters[i].characterType == CharacterType.Player && activeCharacters[i].currentHP > 0)
			{
				// Add i to the players list
				players.Add(i);
			}
		}

		// Choose a random target
		int selectedTarget = players[Random.Range(0, players.Count)];

		// Select an attack to use
		int selectAttack = Random.Range(0, activeCharacters[currentTurn].availableAttacks.Length);

		// Variable to hold the attack's power
		int attackPower = 0;

		// Variable to hold the attack's type
		AttackType attackType = AttackType.Physical;

		// Loop through the moves list
		for (int i = 0; i < battleAttacks.Length; i++)
		{
			// Make sure the name of the attack in the list matches the name of the selected attack
			if (battleAttacks[i].attackName == activeCharacters[currentTurn].availableAttacks[selectAttack])
			{
				// Show the attack animation
				Instantiate(battleAttacks[i].battleAnimation, activeCharacters[selectedTarget].transform);

				// Set the value of attackPower
				attackPower = battleAttacks[i].movePower;

				// Set the attack type
				attackType = battleAttacks[i].attackType;
			}
		}

		// Show the attack effect
		Instantiate(enemyAttackEffect, activeCharacters[currentTurn].transform.position, activeCharacters[selectedTarget].transform.rotation);

		// Deal the attack damage
		DealDamage(selectedTarget, activeCharacters[currentTurn].availableAttacks[selectAttack], attackPower, attackType);
	}

	/// <summary>
	/// Function to inflict damage
	/// </summary>
	/// <param name="attackTarget">The index of the attack target</param>
	/// <param name="attackName">The name of the attack being used</param>
	/// <param name="attackPower">The power of the attack being used</param>
	/// <param name="attackType">The type of the attack being used</param>
	public void DealDamage(int attackTarget, string attackName, int attackPower, AttackType attackType)
	{
		// Damage calculation variables
		float damagePower, damageResist, damage;
		int returnDamage;

		// Calculate damage differently depending on the attack type
		if (attackType == AttackType.Physical)
		{
			// Calculate power
			damagePower = activeCharacters[currentTurn].pAttack + activeCharacters[currentTurn].weaponPower;

			// Calculate resistance
			damageResist = activeCharacters[attackTarget].pDefence + activeCharacters[attackTarget].armourPower;

			// Calculate damage based on power and resistance
			damage = (damagePower / damageResist) * attackPower * Random.Range(1.0f, 2.0f);

			// Round damage to an int and assign it to returnDamage
			returnDamage = Mathf.RoundToInt(damage);

			// Damage the target character
			activeCharacters[attackTarget].currentHP -= returnDamage;

			// Show the damage number on the screen
			Instantiate(damageNumber, activeCharacters[attackTarget].transform.position, activeCharacters[attackTarget].transform.rotation).SetDamage(returnDamage);

			Debug.Log(activeCharacters[currentTurn].characterName + " attacked " + activeCharacters[attackTarget].characterName + " with " + attackName + ", dealing " + returnDamage + " damage");
		}
		else if (attackType == AttackType.Magical)
		{
			// Calculate power
			damagePower = activeCharacters[currentTurn].mAttack + activeCharacters[currentTurn].weaponMagic;

			// Calculate resistance
			damageResist = activeCharacters[attackTarget].mDefence + activeCharacters[attackTarget].armourMagic;

			// Calculate damage based on power and resistance
			damage = (damagePower / damageResist) * attackPower * Random.Range(1.0f, 2.0f);

			// Round damage to an int and assign it to returnDamage
			returnDamage = Mathf.RoundToInt(damage);

			// Damage the target character
			activeCharacters[attackTarget].currentHP -= returnDamage;
			
			// Show the damage number on the screen
			Instantiate(damageNumber, activeCharacters[attackTarget].transform.position, activeCharacters[attackTarget].transform.rotation).SetDamage(returnDamage);

			Debug.Log(activeCharacters[currentTurn].characterName + " attacked " + activeCharacters[attackTarget].characterName + " with " + attackName + ", dealing " + returnDamage + " damage");
		}
		else if (attackType == AttackType.Status)
		{

		}
		else if (attackType == AttackType.Stat)
		{

		}
	}

	/// <summary>
	/// Function to update the stats in the UI
	/// </summary>
	public void UpdateUIStats()
	{
		// Loop through the array
		for (int i = 0; i < playerName.Length; i++)
		{
			if (activeCharacters.Count > i)
			{
				if (activeCharacters[i].characterType == CharacterType.Player)
				{
					BattleCharacter playerData = activeCharacters[i];
					playerName[i].gameObject.SetActive(true);
					playerName[i].text = playerData.characterName;
					playerHP[i].gameObject.SetActive(true);
					playerHP[i].text = Mathf.Clamp(playerData.currentHP, 0, playerData.maximumHP)  + "/" + playerData.maximumHP;
					playerMP[i].gameObject.SetActive(true);
					playerMP[i].text = Mathf.Clamp(playerData.currentMP, 0, playerData.maximumMP) + "/" + playerData.maximumMP;
				}
				else
				{
					playerName[i].gameObject.SetActive(false);
				}
			}
			else
			{
				playerName[i].gameObject.SetActive(false);
			}
		}
	}

	/// <summary>
	/// Function to make the player attack
	/// </summary>
	/// <param name="moveName">The name of the move being used</param>
	/// <param name="selectedTarget">Index of the attack target</param>
	public void PlayerAttack(string moveName, int selectedTarget)
	{
		// Variable to hold the attack's power
		int attackPower = 0;

		// Variable to hold the attack's type
		AttackType attackType = AttackType.Physical;

		// Loop through the moves list
		for (int i = 0; i < battleAttacks.Length; i++)
		{
			// Make sure the name of the attack in the list matches the name of the selected attack
			if (battleAttacks[i].attackName == moveName)
			{
				// Show the attack animation
				Instantiate(battleAttacks[i].battleAnimation, activeCharacters[selectedTarget].transform);

				// Set the value of attackPower
				attackPower = battleAttacks[i].movePower;

				// Set the attack type
				attackType = battleAttacks[i].attackType;
			}
		}

		// Show the attack effect
		Instantiate(enemyAttackEffect, activeCharacters[currentTurn].transform.position, activeCharacters[selectedTarget].transform.rotation);

		// Deal the damage from the attack
		DealDamage(selectedTarget, moveName, attackPower, attackType);

		// Hide the action buttons window
		actionButtonsWindow.SetActive(false);

		// Hide the target window
		targetWindow.SetActive(false);

		// Move to the next turn
		ChangeTurns();
	}

	/// <summary>
	/// Function to make the attack target window visible 
	/// </summary>
	/// <param name="moveName">The name of the variable</param>
	public void ShowTargetWindow(string moveName)
	{
		// Show the target menu
		targetWindow.SetActive(true);

		// Temp list
		List<int> Enemies = new List<int>();

		// Loop through the array
		for (int i = 0; i < activeCharacters.Count; i++)
		{
			// Check if the character at i is an enemy
			if (activeCharacters[i].characterType == CharacterType.Enemy)
			{
				// Add the index number to the list
				Enemies.Add(i);
			}
		}

		// Loop through the array
		for (int i = 0; i < targetButtons.Length; i++)
		{
			// If there are still elements in the array
			if (Enemies.Count > i)
			{
				// Make the corresponding button visible
				targetButtons[i].gameObject.SetActive(true);
				
				// Set the move name in the corresponding button
				targetButtons[i].moveName = moveName;

				// Set the index of the corresponding enemy
				targetButtons[i].activeBattlerTarget = Enemies[i];

				// 
				targetButtons[i].targetName.text = activeCharacters[Enemies[i]].characterName;
			}
			else
			{
				// Make all other buttons invisible
				targetButtons[i].gameObject.SetActive(false);
			}
		}
	}
	
	/// <summary>
	/// Function to open up the magic window
	/// </summary>
	public void ShowMagicWindow()
	{
		// Show the magic menu
		magicWindow.SetActive(true);

		// Loop through the array
		for (int i = 0; i < magicButtons.Length; i++)
		{
			// Make sure i is within the number of spells the user has
			if (activeCharacters[currentTurn].availableAttacks.Length > i)
			{
				// Show the corresponding button
				magicButtons[i].gameObject.SetActive(true);

				// Set the name of the spell to the button
				magicButtons[i].spellName = activeCharacters[currentTurn].availableAttacks[i];

				// Set the name of the spell to the UI
				magicButtons[i].spellNameText.text = magicButtons[i].spellName;

				// Loop through the moves list
				for (int j = 0; j < battleAttacks.Length; j++)
				{
					// Make sure the name of the attack at j matches the name of the magic button at i
					if (battleAttacks[j].attackName == magicButtons[i].spellName)
					{
						// Set the MP cost of the spell to the button
						magicButtons[i].spellCost = battleAttacks[j].moveCost;

						// Set the MP cost of the spell to the UI
						magicButtons[i].spellCostText.text = magicButtons[i].spellCost.ToString();
					}
				}
			}
			else
			{
				// Hide all other buttons
				magicButtons[i].gameObject.SetActive(false);
			}
		}
	}

	/// <summary>
	/// Function to flee the battle 
	/// </summary>
	public void Flee()
	{
		// Temporary list of enemy speeds
		List<int> enemySpeeds = new List<int>();

		// Loop through the active characters
		for (int i = 0; i < activeCharacters.Count; i++)
		{
			// If the character at i is an enemy
			if (activeCharacters[i].characterType == CharacterType.Enemy)
			{
				// Add the speeds to the list
				enemySpeeds.Add(activeCharacters[i].speed);

				// Sort the list
				enemySpeeds.Sort((a, b) => -1 * a.CompareTo(b));
			}
		}

		for (int i = 0; i < enemySpeeds.Count; i++)
		{
			Debug.Log(enemySpeeds[i].ToString());
		}

		// Loop through the speeds list
		for (int i = 0; i < enemySpeeds.Count; i++)
		{
			// If the player whose current turn it is has higher speed than the enemy at i
			if (activeCharacters[currentTurn].speed > enemySpeeds[i])
			{
				// Display the message saying they escaped
				battleMessage.ShowMessage("Got away safely!");

				// Flee the battle
				Debug.Log("Flee the battle.");
			}
			else
			{
				// Display the message saying they cannot escape
				battleMessage.ShowMessage("Can't get away!");

				// Return out of the function
				return;
			}
		}
	}
}

public enum TurnStates
{
	Idle,
	Active
}
