﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageNumber : MonoBehaviour
{
	/* Public Variables */
	public Text damageText;
	public float lifeTime = 1.0f;
	public float moveSpeed = 1.0f;
	public float placementJitter = 0.5f;

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		Destroy(gameObject, lifeTime);
		transform.position += new Vector3(0, moveSpeed * Time.deltaTime, 0);
	}

	public void SetDamage(int damageNumber)
	{
		damageText.text = damageNumber.ToString();
		transform.position += new Vector3(Random.Range(-placementJitter, placementJitter), Random.Range(-placementJitter, placementJitter), 0);
	}
}
