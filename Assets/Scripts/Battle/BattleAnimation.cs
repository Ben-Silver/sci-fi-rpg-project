﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleAnimation : MonoBehaviour
{
	/* Public Variables */
	public float animationLength;
	public int SFX;

	// Use this for initialization
	void Start()
	{
		AudioManager.instance.PlayeSFX(SFX);
	}
	
	// Update is called once per frame
	void Update()
	{
		Destroy(gameObject, animationLength);
	}
}
