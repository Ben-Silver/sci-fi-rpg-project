﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyOverworld : MonoBehaviour
{
	public bool inRange;
	public bool battled;
	public int battleMusic;
	public string[] enemies;

	// Use this for initialization
	void Start()
	{
		enemies = new string[]
		{
			"Eyeball", "Eyeball"
		};

		battleMusic = 0;
	}
	
	// Update is called once per frame
	void Update()
	{
		if (Input.GetButtonDown("Fire1") && inRange && !battled)
		{
			BattleManager.instance.BattleStart(enemies);
		}
	}

	// When an object enters this object's proximity
	private void OnTriggerEnter2D(Collider2D other)
	{
		// Check to see if the collider is the player
		if (other.gameObject.tag == "Player" && !battled)
		{
			// The player is now in range to open the chest
			inRange = true;
		}
	}

	// When an object exits this object's proximity
	private void OnTriggerExit2D(Collider2D other)
	{
		// Check to see if the collider is the player
		if (other.gameObject.tag == "Player" && !battled)
		{
			// The player is no-longer in range to open the chest
			inRange = false;
		}
	}
}
