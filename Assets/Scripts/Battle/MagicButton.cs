﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MagicButton : MonoBehaviour
{
	/* Public Variables */
	public string spellName;
	public int spellCost;
	public Text spellNameText;
	public Text spellCostText;

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}

	/// <summary>
	/// Function for whent the magic button is pressed
	/// </summary>
	public void Press()
	{
		// Make sure the user has enough MP to cast magic
		if (BattleManager.instance.activeCharacters[BattleManager.instance.currentTurn].currentMP >= spellCost)
		{
			// Hide the magic menu
			BattleManager.instance.magicWindow.SetActive(false);

			// Show the target menu
			BattleManager.instance.ShowTargetWindow(spellName);

			// Subtract the cost of the spell from the user's MP
			BattleManager.instance.activeCharacters[BattleManager.instance.currentTurn].currentMP -= spellCost;
		}
		else
		{
			BattleManager.instance.battleMessage.ShowMessage("Not Enough MP.");
			Debug.Log(BattleManager.instance.battleMessage.battleMessage.text);
			BattleManager.instance.magicWindow.SetActive(false);
		}
	}
}
