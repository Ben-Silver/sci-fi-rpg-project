﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOverLifetime : MonoBehaviour
{
	/* Public Variables */
	public float lifeTime = 2.0f;

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		Destroy(gameObject, lifeTime);
	}
}
