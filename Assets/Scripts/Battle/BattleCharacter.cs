﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleCharacter : MonoBehaviour
{
	/* Public Variables */
	[Header("States")]
	public CharacterType characterType;
	public CharacterState characterState;

	[Header("Stats")]
	public string characterName;
	public int currentHP;
	public int maximumHP;
	public int currentMP;
	public int maximumMP;
	public int currentLevel;
	public int pAttack;
	public int pDefence;
	public int mAttack;
	public int mDefence;
	public int speed;
	public int weaponPower;
	public int weaponMagic;
	public int armourPower;
	public int armourMagic;
	public int armourSpeed;

	[Header("Skills")]
	public string[] availableAttacks;

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}
}

public enum CharacterType
{
	Player,
	Enemy,
}

public enum CharacterState
{
	Active,
	Inactive,
}