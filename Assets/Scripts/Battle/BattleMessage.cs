﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleMessage : MonoBehaviour
{
	/* Public Variables */
	public Text battleMessage;

	/* Private Variables */
	private float lifespan = 2.0f;
	private float lifespanCounter = 0;

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		// Check to see if the lifespan has reached 0
		if (lifespan <= lifespanCounter || Input.GetButtonDown("Fire1"))
		{
			// Set this to be inactive
			gameObject.SetActive(false);
		}
		else if (lifespan > lifespanCounter)
		{
			// Decrease lifespan in real-time
			lifespan -= Time.deltaTime;
		}
	}

	/// <summary>
	/// Function to show a message
	/// </summary>
	/// <param name="message">The message to display</param>
	public void ShowMessage(string message)
	{
		// Make this visible
		gameObject.SetActive(true);

		// Pass the message to the UI
		battleMessage.text = message;

		// Reset the lifespan
		lifespan = 2.0f;
	}
}
