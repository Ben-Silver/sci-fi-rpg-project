﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetButton : MonoBehaviour
{
	/* Public Variables */
	public string moveName;
	public int activeBattlerTarget;
	public Text targetName;

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}

	/// <summary>
	/// Function that triggers when a UI button is pressed
	/// </summary>
	public void Press()
	{
		// Launch an attack
		BattleManager.instance.PlayerAttack(moveName, activeBattlerTarget);
	}
}
