﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItemSlot : MonoBehaviour
{
	/* Public Variables */
	public Text itemNameText;
	public Image itemSprite;
	public Text itemQuanity;
	public int buttonIndex;

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}

	/// <summary>
	/// Function for when the shopItemSlot is selected
	/// </summary>
	public void Press()
	{
		// Select the item in the shop menu
		Shop.instance.SelectSaleItem(Shop.instance.itemsForSale[buttonIndex]);
	}

	public void PressSell()
	{
		// Select the item in the shop menu
		Shop.instance.SelectSaleItem(Shop.instance.itemsToSell[buttonIndex]);
	}
}
