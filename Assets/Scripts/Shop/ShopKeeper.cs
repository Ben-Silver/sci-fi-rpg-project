﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopKeeper : MonoBehaviour
{
	/* Public Variables */
	[Header("Activator Variable")]
	public bool inRange;

	[Header("Items for Sale")]
	public List<Item> itemsForSale;

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		// If the player is in range and clicking the mouse
		if (Input.GetButtonDown("Fire1") && inRange && !GameManager.instance.gameIsBusy && PlayerController.instance.direction == Direction.Up)
		{
			// Open the shop
			Shop.instance.OpenShop();

			// Set the list of items for sale
			Shop.instance.itemsForSale = itemsForSale;
		}
	}

	// When an object enters the proximity of this object
	private void OnTriggerEnter2D(Collider2D other)
	{
		// Set inRange based on whether the intruding object has the Player tag
		inRange = (other.gameObject.tag == "Player") ? true : false;
	}

	// When an object leaves the proximity of this object
	private void OnTriggerExit2D(Collider2D other)
	{
		// Set inRange to false
		inRange =  false;
	}
}
