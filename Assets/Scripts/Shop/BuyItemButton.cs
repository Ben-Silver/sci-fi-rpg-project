﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyItemButton : MonoBehaviour
{
	/* Public Variables */
	public Item selectedItem;

	// When the button is pressed
	public void Press()
	{
		// Buy the item
		Shop.instance.BuySaleItem(selectedItem);

		// Check to see if the item can be bought
		if (GameManager.instance.currentGold < selectedItem.itemValue)
		{
			// Remove the selected item
			selectedItem = null;
		}
	}
}
