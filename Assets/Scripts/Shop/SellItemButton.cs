﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SellItemButton : MonoBehaviour
{
	/* Public Variables */
	public Item selectedItem;

	// When the button is pressed
	public void Press()
	{
		// Sell the selected item
		Shop.instance.SellItem(selectedItem);

		// Remove the selected item
		selectedItem = null;

		// Remove button interactability
		gameObject.GetComponent<Button>().interactable = false;
	}
}
