﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
	/* Public Variables */
	public GameObject shopMenu;
	public GameObject buyMenu;
	public GameObject sellMenu;
	public Text goldText;
	public Text itemNameBuy,
                itemDescriptionBuy,
				itemPriceBuy,
				itemNameSell,
                itemDescriptionSell,
				itemPriceSell;
	public Button buyButton,
				  sellButton;
	public List<Item> itemsForSale;
	public List<Item> itemsToSell;
	public ShopItemSlot[] shopItemSlots;
	public ShopItemSlot[] sellItemSlots;
	public BuyItemButton buyItemButton;
	public SellItemButton sellItemButton;

	/* Private Variables */
	public Item selectedItem;
	
	#region Singleton
	public static Shop instance;

	void Awake()
	{
		// Check to see if there is already an instance of the gameObject in the scene
		if (instance == null)
		{
			// Set the instance to this gameObject
			instance = this;
		}
		else
		{
			// Remove unwanted instances
			Destroy(gameObject);
		}

		// Make sure the gameObject isn't destroyed when a new level is loaded
		DontDestroyOnLoad(gameObject);
	}
	#endregion

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		// Get the current gold value
		goldText.text = GameManager.instance.GetGold().ToString() + " G";
	}

	/// <summary>
	/// Function to open the shop menu
	/// </summary>
	public void OpenShop()
	{
		// Make the shop menu visible
		shopMenu.SetActive(true);

		// Let the GameManager know the shop is active
		GameManager.instance.shopOpen = true;
	}

	/// <summary>
	/// Function to close the shop menu
	/// </summary>
	public void CloseShop()
	{
		// Make the shop menu invisible
		shopMenu.SetActive(false);

		// Let the GameManager know the shop is inactive
		GameManager.instance.shopOpen = false;

		// Clear the items for sale list
		//itemsForSale.Clear();

		// Make the buy menu invisible
		buyMenu.SetActive(false);

		// Make the sell menu invisible
		sellMenu.SetActive(false);
	}

	/// <summary>
	/// Function to open the buy menu
	/// </summary>
	public void OpenBuyMenu()
	{
		// Show the items for sale
		ShowItemsForSale();

		// Make the buy menu visible
		buyMenu.SetActive(true);

		// Make the sell menu invisible
		sellMenu.SetActive(false);
	}

	/// <summary>
	/// Function to open the sell menu
	/// </summary>
	public void OpenSellMenu()
	{
		// Get the item inventory
		itemsToSell = GameManager.instance.itemInventory;

		// Make the shop menu visible
		sellMenu.SetActive(true);
		
		// Make the buy menu invisible
		buyMenu.SetActive(false);

		// Loop through the array
        for (int i = 0; i < sellItemSlots.Length; i++)
        {
            // Set the index of each button
            sellItemSlots[i].buttonIndex = i;

			// Make the sellItemSlots uninteractable
			sellItemSlots[i].GetComponent<Button>().interactable = false;

            // Set all buttons to be invisible
            sellItemSlots[i].gameObject.SetActive(false);
        }

        // Loop through the list
        for (int i = 0; i < GameManager.instance.itemInventory.Count; i++)
        {
            // Make all occupied slots visible
            sellItemSlots[i].gameObject.SetActive(true);

			// Make the sellItemSlots interactable
			sellItemSlots[i].GetComponent<Button>().interactable = true;

            // Set the sprite of the item in the slot
            sellItemSlots[i].itemSprite.sprite = GameManager.instance.itemInventory[i].itemSprite;

            // Set the quantity of the item in the slot
            sellItemSlots[i].itemQuanity.text = GameManager.instance.itemQuantity[i].ToString();
        }
	}

	/// <summary>
	/// Function to show all the items for sale
	/// </summary>
	public void ShowItemsForSale()
	{
		// Loop through the array
        for (int i = 0; i < shopItemSlots.Length; i++)
        {
            // Set the index of each button
            shopItemSlots[i].buttonIndex = i;

			// Make the shopItemSlots uninteractable
			shopItemSlots[i].GetComponent<Button>().interactable = false;

            // Set all buttons to be invisible
            shopItemSlots[i].gameObject.SetActive(false);
        }

        // Loop through the list
        for (int i = 0; i < itemsForSale.Count; i++)
        {
            // Make all occupied slots visible
            shopItemSlots[i].gameObject.SetActive(true);

			// Make the shopItemSlots interactable
			shopItemSlots[i].GetComponent<Button>().interactable = true;

            // Set the name of the item in the slot
            shopItemSlots[i].itemNameText.text = itemsForSale[i].itemName;

            // Set the sprite of the item in the slot
            shopItemSlots[i].itemSprite.sprite = itemsForSale[i].itemSprite;
        }
	}

	/// <summary>
	/// Function to select an item when the button is clicked
	/// </summary>
	/// <param name="saleItem">The item contained in the ShopItemSlot</param>
	public void SelectSaleItem(Item saleItem)
	{
		// Set the selected item to the item passed in
        selectedItem = saleItem;

		// Check to see if the player is buying or selling
		if (buyMenu.activeInHierarchy)
		{
			// Show the name of the item in the interface
			itemNameBuy.text = selectedItem.itemName;

			// Show the description of the item in the interface
			itemDescriptionBuy.text = selectedItem.itemDescription;

			// Show the price of the item in the interface
			itemPriceBuy.text = "Price: " + selectedItem.itemValue.ToString() + " G";

			// Set the selected item for the buyItemButton
			buyItemButton.selectedItem = saleItem;

			// Check to see if the item can be bought
			if (GameManager.instance.currentGold >= selectedItem.itemValue)
			{
				// Enable the buy button
				buyButton.interactable = true;
			}
			else
			{
				// Disable the buy button
				buyButton.interactable = false;
			}
		}
		else if (sellMenu.activeInHierarchy)
		{
			// Show the name of the item in the interface
			itemNameSell.text = selectedItem.itemName;

			// Show the description of the item in the interface
			itemDescriptionSell.text = selectedItem.itemDescription;

			// Show the price of the item in the interface
			itemPriceSell.text = "Price: " + selectedItem.itemSaleValue.ToString() + " G";

			// Set the selected item for the buyItemButton
			sellItemButton.selectedItem = saleItem;
			sellButton.interactable = true;
		}

		
	}

	/// <summary>
	/// Function to buy an item
	/// </summary>
	/// <param name="saleItem">The item being bought</param>
	public void BuySaleItem(Item saleItem)
	{
		// Make sure an item is selected
		if (saleItem != null)
		{
			// Buy the item
			saleItem.BuyItem();

			// Check to see if the item can be bought
			if (GameManager.instance.currentGold >= saleItem.itemValue)
			{
				// Enable the buy button
				buyButton.interactable = true;
			}
			else
			{
				// Disable the buy button
				buyButton.interactable = false;
			}
		}
	}

	public void SellItem(Item saleItem)
	{
		// Sell the item
		GameManager.instance.AddGold(saleItem.itemSaleValue);

		// Remove the item from the inventory
		GameManager.instance.RemoveItem(saleItem);

		// Update the itemsToSell
		itemsToSell = GameManager.instance.itemInventory;

		// Loop through the array
        for (int i = 0; i < sellItemSlots.Length; i++)
        {
            // Set the index of each button
            sellItemSlots[i].buttonIndex = i;

			// Make the sellItemSlots uninteractable
			sellItemSlots[i].GetComponent<Button>().interactable = false;

            // Set all buttons to be invisible
            sellItemSlots[i].gameObject.SetActive(false);
        }

        // Loop through the list
        for (int i = 0; i < GameManager.instance.itemInventory.Count; i++)
        {
            // Make all occupied slots visible
            sellItemSlots[i].gameObject.SetActive(true);

			// Make the sellItemSlots interactable
			sellItemSlots[i].GetComponent<Button>().interactable = true;

            // Set the sprite of the item in the slot
            sellItemSlots[i].itemSprite.sprite = GameManager.instance.itemInventory[i].itemSprite;

            // Set the quantity of the item in the slot
            sellItemSlots[i].itemQuanity.text = GameManager.instance.itemQuantity[i].ToString();
        }
	}
}
