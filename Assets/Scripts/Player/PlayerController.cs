﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	/* Public Variables */
	public Direction direction;
	public Vector2 lastMoveDir;
	public bool playerMoving;
	public bool playerCanMove = true;
	public string mapDestinationPoint;

	/* Private Variables */
	private Rigidbody2D rb;
	private Animator anim;
	private int moveSpeed = 6;
	private int speedModifier = 2;
	private Vector3 bottomLeftLimit;
	private Vector3 topRightLimit;

	#region Singleton
	public static PlayerController instance;

	void Awake()
	{
		// Check to see if there is already an instance of the gameObject in the scene
		if (instance == null)
		{
			// Set the instance to this gameObject
			instance = this;
		}
		else
		{
			// Remove unwanted instances
			Destroy(gameObject);
		}

		// Make sure the gameObject isn't destroyed when a new level is loaded
		DontDestroyOnLoad(gameObject);
	}
	#endregion

	// Use this for initialization
	void Start()
	{
		// Get the Rigidbody2D component and assign it to rb
		rb = GetComponent<Rigidbody2D>();

		// Get the Animator component and assign it to anim
		anim = GetComponent<Animator>();

		// Set the last moved direction
        lastMoveDir = new Vector2(0.0f, -1.0f);
	}
	
	// Update is called once per frame
	void Update()
	{
		// Player movement
		MovePlayer();

		// Update the Animator
		UpdateAnimator();

		UpdateDirection();

		// Ensure the player stays within the bounds of the map
		transform.position = new Vector3(Mathf.Clamp(transform.position.x, bottomLeftLimit.x, topRightLimit.x), 
										 Mathf.Clamp(transform.position.y, bottomLeftLimit.y, topRightLimit.y), 
										 transform.position.z);
	}

	///<Summary>
	/// Player movement
	///</Summary>
	public void MovePlayer()
	{		
		// Set the player's velocity based on the vertical/horizontal input, modified by whether the run button is being pressed
		rb.velocity = Input.GetButton("Run") ? new Vector2(Input.GetAxisRaw("Horizontal") * moveSpeed * speedModifier, Input.GetAxisRaw("Vertical") * moveSpeed * speedModifier) :
											   new Vector2(Input.GetAxisRaw("Horizontal") * moveSpeed, Input.GetAxisRaw("Vertical") * moveSpeed);
		
		// Check to see if the player can move
		if (!playerCanMove)
		{
			// Stop the player from moving
			rb.velocity = Vector2.zero;
			playerMoving = false;
		}

		// Update the last direction moved
		if (rb.velocity != Vector2.zero)
		{
			// Set the last moved direction
			lastMoveDir = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
		}
	}

	///<Summary>
	/// Update the Animator component on the player
	///</Summary>
	public void UpdateAnimator()
	{
		// Set playerMoving according to whether the player's velocity is not 0
		playerMoving = (rb.velocity != Vector2.zero) ? true : false;

		// Reference the animator to say the sprite is moving
		anim.SetBool("playerMoving", playerMoving);

		// Reference the animator to update the sprite
		anim.SetFloat("moveX", Input.GetAxisRaw("Horizontal"));
		anim.SetFloat("moveY", Input.GetAxisRaw("Vertical"));

		// Update the animator blend tree with current direction of player
		anim.SetFloat("lastMoveX", lastMoveDir.x);
		anim.SetFloat("lastMoveY", lastMoveDir.y);
	}

	/// <summary>
	/// Function to update the player's direction in an understandable manner
	/// </summary>
	public void UpdateDirection()
	{
		// Up
		if (lastMoveDir.x == 0 && lastMoveDir.y == 1)
		{
			direction = Direction.Up;
		}

		// Down
		if (lastMoveDir.x == 0 && lastMoveDir.y == -1)
		{
			direction = Direction.Down;
		}

		// Left
		if (lastMoveDir.x == -1 && lastMoveDir.y == 0)
		{
			direction = Direction.Left;
		}

		// Right
		if (lastMoveDir.x == 1 && lastMoveDir.y == 0)
		{
			direction = Direction.Right;
		}

		// Up Right
		if (lastMoveDir.x == 1 && lastMoveDir.y == 1)
		{
			direction = Direction.UpRight;
		}

		// Down Right
		if (lastMoveDir.x == 1 && lastMoveDir.y == -1)
		{
			direction = Direction.DownRight;
		}

		// Up Left
		if (lastMoveDir.x == -1 && lastMoveDir.y == 1)
		{
			direction = Direction.UpLeft;
		}

		// Down Left
		if (lastMoveDir.x == -1 && lastMoveDir.y == -1)
		{
			direction = Direction.DownLeft;
		}
	}

	///<summary>
	/// Function to set the bounds for the player
	///</summary>
	/// <param name="bottomLeft">The bottom left bounds of the screen</param>
	/// <param name="topRight">The top right bounds of the screen</param>
	public void SetBounds(Vector3 bottomLeft, Vector3 topRight)
	{
		// 
		bottomLeftLimit = bottomLeft + new Vector3(1, 1, 0);
		topRightLimit = topRight + new Vector3(-1, -1, 0);
	}
}

public enum Direction
{
	Up,
	Down,
	Left,
	Right,
	UpLeft,
	UpRight,
	DownLeft,
	DownRight
}