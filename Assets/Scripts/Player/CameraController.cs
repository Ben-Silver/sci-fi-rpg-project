﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CameraController : MonoBehaviour
{
	/* Public Variables */
	public Transform target;
	public Tilemap map;
	public int bgmIndex;
	public int sfxIndex;

	/* Private Variables */
	private Vector3 bottomLeftLimit;
	private Vector3 topRightLimit;
	private float halfWidth;
	private float halfHeight;
	private bool bgmPlaying;

	// Use this for initialization
	void Start()
	{
		// Set the target to the transform of the player
		target = PlayerController.instance.transform;

		// Set the half height
		halfHeight = Camera.main.orthographicSize;
		
		// Set the half width
		halfWidth = halfHeight * Camera.main.aspect;

		// Get the upper and lower limits of the map
		bottomLeftLimit = map.localBounds.min + new Vector3(halfWidth, halfHeight, 0);
		topRightLimit = map.localBounds.max + new Vector3(-halfWidth, -halfHeight, 0);

		// Set the bounds for the player
		PlayerController.instance.SetBounds(map.localBounds.min, map.localBounds.max);
	}
	
	// LateUpdate is called once per frame after Update
	void LateUpdate()
	{
		// Make the camera follow the player
		transform.position = new Vector3(target.position.x, target.position.y, target.position.z - 10);

		// Ensure the camera stays within the bounds of the map
		transform.position = new Vector3(Mathf.Clamp(transform.position.x, bottomLeftLimit.x, topRightLimit.x), 
										 Mathf.Clamp(transform.position.y, bottomLeftLimit.y, topRightLimit.y), 
										 transform.position.z);

		// See if music is currently playing
		if (!bgmPlaying)
		{
			// Play the specified BGM
			AudioManager.instance.PlayBGM(bgmIndex);

			// Set bgmPlaying to true
			bgmPlaying = true;
		}
	}
}
