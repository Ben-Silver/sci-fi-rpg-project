﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterStats : MonoBehaviour
{
	/* Public Variables */
	public string charName;
	public int currentLevel = 1;
	public int maximumLevel = 100;
	public int currentEXP;
	public int baseEXP = 20;
	public int currentHP;
	public int maximumHP = 100;
	public int currentMP;
	public int maximumMP = 40;
	public int pAttack = 35;
	public int pDefence = 30;
	public int mAttack = 33;
	public int mDefence = 27;
	public int speed = 32;
	public int weaponPower;
	public int weaponMagic;
	public int armourPower;
	public int armourMagic;
	public int armourSpeed;
	public Item equippedWeapon;
	public Item equippedArmour;
	public Sprite charSprite;
	public int[] expToNextLevel;
	public int[] mpLevelBonus;

	// Use this for initialization
	void Start()
	{
		// Set the expToNextLevel values
		CalculateEXPToNextLevel();

		// Set the mpLevelBonus values
		CalculateMPLevelBonus();
	}
	
	// Update is called once per frame
	void Update()
	{

	}


	///<Summary>
	/// Function to add exp to the current exp
	///</Summary>
	public void AddEXP(int addEXP)
	{
		// Add the value passed in to the currentEXP
		currentEXP += (currentLevel < maximumLevel) ? addEXP : 0;

		if (currentLevel < maximumLevel)
		{
			// Check to see if the currentEXP exceeds the level up threshold
			if (currentEXP >= expToNextLevel[currentLevel])
			{
				// Subtract the residual exp from the level up
				currentEXP -= expToNextLevel[currentLevel];
				
				// Increase the player's level as long as it hasn't reached it's maximum
				currentLevel += 1;
				
				// Calculate stat bonuses
				currentHP += CalculateStatIncrease(maximumHP + 2/currentLevel);
				maximumHP += CalculateStatIncrease(maximumHP + 2/currentLevel);
				pAttack += CalculateStatIncrease(pAttack);
				pDefence += CalculateStatIncrease(pDefence);
				mAttack += CalculateStatIncrease(mAttack);
				mDefence += CalculateStatIncrease(mDefence);
				speed += CalculateStatIncrease(speed);
				currentMP += (currentLevel == maximumLevel) ? mpLevelBonus[currentLevel - 1] : mpLevelBonus[currentLevel];
				maximumMP += (currentLevel == maximumLevel) ? mpLevelBonus[currentLevel - 1] : mpLevelBonus[currentLevel];
			}
		}
	}

	///<Summary>
	/// Function to calculate each value of expToNextLevel array
	///</Summary>
	void CalculateEXPToNextLevel()
	{
		// Initialise expToNextLevel with length of maximumLevel
		expToNextLevel = new int[maximumLevel];

		// Loop through the array
		for (int i = 0; i < expToNextLevel.Length; i++)
		{
			// Calculate expToNextLevel at each index
			expToNextLevel[i] = baseEXP + Mathf.RoundToInt(Mathf.Pow(i * 2, 2));
		}
	}

	///<Summary>
	/// Function to calculate each value of expToNextLevel array
	///</Summary>
	void CalculateMPLevelBonus()
	{
		// Initialise mpLevelBonus with length of maximumLevel
		mpLevelBonus = new int[maximumLevel];

		// Loop through the array
		for (int i = 0; i < mpLevelBonus.Length; i++)
		{
			// Calculate mpLevelBonus at each index with an even number
			mpLevelBonus[i] = ((i + 1) % 2 == 0) ? 2 * i + CalculateStatIncrease(2 * ((2 * i) / (i + 1)) - 1/(i + 2)) - (4 * i) + (3 * i) : 0;
		}
	}

	///<Summary>
	/// Function to calculate stat changes on levelling up
	///</Summary>
	int CalculateStatIncrease(int stat)
	{
		if (currentLevel < maximumLevel)
		{
			float increase = ((expToNextLevel[currentLevel] + 2) / (stat * currentLevel)) + 1 + (currentLevel/stat);
			//Debug.Log(stat.ToString() + " + " + increase.ToString());
			return (increase < 1) ? 1 : Mathf.RoundToInt(increase);
		}
		else
		{
			return Mathf.RoundToInt((currentLevel / 75) + 2) + (stat * (currentLevel / 75)) + 1 - ((currentLevel / 75) / (stat));
		}
	}
}

/*Pokemon EXP calculation
int A = (opponentLevel * 2) + 10;
int B;
int C = opponentLevel + UserLevel + 10;
int D = Mathf.FloorToInt(Mathf.FloorToInt(Mathf.Sqrt(A * (A * A)) * B / Mathf.FloorToInt(Mathf.Sqrt(C * (C * C))))) + 1;*/