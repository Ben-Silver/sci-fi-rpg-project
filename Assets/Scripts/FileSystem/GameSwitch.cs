﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameSwitch
{
    /* Public Variables */
    public string switchName;                       // List containing the names of the GameSwitches
    public bool switchValue;                        // List containing the boolean values of the GameSwitches

    // Constructor
    public GameSwitch(string sName, bool sValue)
    {
        // Set switchName to the name passed in
        switchName = sName;

        // Set switchValue to the name passed in
        switchValue = sValue;

        Debug.Log("Created Switch '" + sName + "' with the value " + sValue);
    }
}
