﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
	/* Public Variables */
	public string mapDestination;
	public string mapDestinationPoint;
	public Direction direction;

	/* Private Variables */
	private LevelChanger levelChanger;

	// Use this for initialization
	void Start()
	{
		// Find the LevelChanger in the scene
		levelChanger = FindObjectOfType<LevelChanger>();
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		// Make sure the Collider2D is the player
		if (other.gameObject.tag == "Player")
		{
			// Transfer the player to the designated level
			levelChanger.FadeToLevel(mapDestination, direction);

			// Set the mapDestinationPoint of the player
			PlayerController.instance.mapDestinationPoint = mapDestinationPoint;
		}
	}
}
