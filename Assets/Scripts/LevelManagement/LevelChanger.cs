﻿/*-------------------------------------------------*/
/*                 LevelChanger.cs                 */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour
{
    /* Public Variables */
    public Animator animator;                   // The animator component on the gameObject

    /* Private Variables */
    private string levelNameToLoad;             // The name of the level to load
    private int levelIndexToLoad;               // The index of the level to load
    private bool loadByName;                    // Determines how the parameters are passed into the scene loader
    private Direction directionToFace;

	// Update is called once per frame
	void Update()
    {

	}

    /// <summary>
    /// Function to fade out and load a level by name
    /// </summary>
    /// <param name="levelName">The name of the level to load</param>
    public void FadeToLevel(string levelName)
    {
        // Assign levelName to levelNameToLoad
        levelNameToLoad = levelName;

        // A level is being loaded by name
        loadByName = true;

        // Check to see if there is an instance of the game manager
        if (GameManager.instance)
        {
            // Stop the player from moving
            GameManager.instance.fadingToLevel = true;
        }

        // Start fading out
        animator.SetTrigger("FadeOut");
    }

    /// <summary>
    /// Function to fade out and load a level by index
    /// </summary>
    /// <param name="levelIndex">The index of the level to load</param>
    public void FadeToLevel(int levelIndex)
    {
        // Assign levelIndex to levelIndexToLoad
        levelIndexToLoad = levelIndex;

        // A level is being loaded by index
        loadByName = false;

        // Stop the player from moving
        GameManager.instance.fadingToLevel = true;

        // Start fading out
        animator.SetTrigger("FadeOut");
    }

   /// <summary>
    /// Function to fade out and load a level by name
    /// </summary>
    /// <param name="levelName">The name of the level to load</param>
    /// <param name="direction">The direction the player will face</param>
    public void FadeToLevel(string levelName, Direction direction)
    {
        // Assign levelName to levelNameToLoad
        levelNameToLoad = levelName;

        // Assign direction to directionToFace
        directionToFace = direction;

        // A level is being loaded by name
        loadByName = true;

        // Check to see if there is an instance of the game manager
        if (GameManager.instance)
        {
            // Stop the player from moving
            GameManager.instance.fadingToLevel = true;
        }

        // Start fading out
        animator.SetTrigger("FadeOut");
    }

    /// <summary>
    /// Function to fade out and load a level by index
    /// </summary>
    /// <param name="levelIndex">The index of the level to load</param>
    /// <param name="direction">The direction the player will face</param>
    public void FadeToLevel(int levelIndex, Direction direction)
    {
        // Assign levelIndex to levelIndexToLoad
        levelIndexToLoad = levelIndex;

        // Assign direction to directionToFace
        directionToFace = direction;

        // A level is being loaded by index
        loadByName = false;

        // Stop the player from moving
        GameManager.instance.fadingToLevel = true;

        // Start fading out
        animator.SetTrigger("FadeOut");
    }


    /// <summary>
    /// Function called in an animation event when the fade out has completed
    /// </summary>
    public void OnFadeComplete()
    {
        // Change the player's direction based on what was passed in
        switch(directionToFace)
        {
            // Make the player face up
            case Direction.Up:
                PlayerController.instance.lastMoveDir = new Vector2(0, 1);
            break;

            // Make the player face down
            case Direction.Down:
                PlayerController.instance.lastMoveDir = new Vector2(0, -1);
            break;

            // Make the player face left
            case Direction.Left:
                PlayerController.instance.lastMoveDir = new Vector2(-1, 0);
            break;

            // Make the player face right
            case Direction.Right:
                PlayerController.instance.lastMoveDir = new Vector2(1, 0);
            break;
        }

        // If the level is being loaded by name
        if (loadByName)
        {
            // Load the level by name
            SceneManager.LoadScene(levelNameToLoad);
        }
        else
        {
            // Load the level by index
            SceneManager.LoadScene(levelIndexToLoad);
        }

        // Check to see if there is an instance of the game manager
        if (GameManager.instance)
        {
            // Allow the player to move again
            GameManager.instance.fadingToLevel = false;
        }

        
    }

    ///
    public void OnLoadComplete()
    {
        // If the game is loading
        if (GameManager.instance && !GameManager.instance.loadingGame)
        {
            // Load the game
            GameManager.instance.LoadData();
        }
    }

    /// <summary>
    /// Returns the name of the currently active scene
    /// </summary>
    public static string GetSceneName()
    {
        // Return the active scene's name and return it
        return SceneManager.GetActiveScene().name;
    }
}
