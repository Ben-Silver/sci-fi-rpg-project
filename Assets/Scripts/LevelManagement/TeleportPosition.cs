﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportPosition : MonoBehaviour
{
	/* Public Variables */
	public string mapDestinationPoint;

	// Use this for initialization
	void Start()
	{
		// Check to see if this mapDestinationPoint matches the player's version
		if (mapDestinationPoint == PlayerController.instance.mapDestinationPoint)
		{
			// Spawn the player at the position of this gameObject
			PlayerController.instance.transform.position = this.transform.position;
		}
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}
}
