﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLoader : MonoBehaviour
{
	/* Public Variables */
	public GameObject player;

	// Use this for initialization
	void Start()
	{
		// Check to see if there's a player in the scene
		if (PlayerController.instance == null)
		{
			// Instantiate a new player
			Instantiate(player);
		}
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}
}
