﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleScreen : MonoBehaviour
{
	/* Public Variables */
	public string startingSceneName;
	public GameObject continueButton;

	/* Private Variables */
	private LevelChanger levelChanger;

	// Use this for initialization
	void Start()
	{
		// Find the LevelChanger in the scene
		levelChanger = FindObjectOfType<LevelChanger>();

		// Make sure PlayerPrefs has data
		if (PlayerPrefs.HasKey("Current_Scene"))
		{
			// Make the button interactible
			continueButton.GetComponent<Button>().interactable = true;
		}
		else
		{
			// Make the button uninteractible
			continueButton.GetComponent<Button>().interactable = false;
		}
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}

	/// <summary>
	/// Function to continue from the current save file
	/// </summary>
	public void Continue()
	{
		// Go to the last map the player was on
		levelChanger.FadeToLevel(PlayerPrefs.GetString("Current_Scene"));
	}

	/// <summary>
	/// Function to start a new game
	/// </summary>
	public void NewGame()
	{
		// Fade to the starting scene
		levelChanger.FadeToLevel(startingSceneName);
	}

	/// <summary>
	/// Function to close the game
	/// </summary>
	public void ExitGame()
	{
		// Exit the game
		Application.Quit();
	}
}
