﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
	/* Public Variables */
    [Header("Pause Menu Components")]
	public GameObject pauseMenu;
	public bool menuOpen = false;

    [Header("Window Components")]
    public GameObject[] windows;

    [Header("Character Stats Components")]
    public CharacterStats[] characterStats;
    public GameObject[] playerStatsBox;
    public Text[] playerNameText,
                  playerLvlText,
                  playerHPText,
                  playerMPText;
    public Slider[] expSlider;
    public Image[] playerPortrait;

    [Header("Party Components")]
    public GameObject[] rosterButtons;
    public Text rosterHPText,
                rosterMPText,
                rosterATKText,
                rosterDEFText,
                rosterMATKText,
                rosterMDEFText,
                rosterSpeedText,
                rosterWeaponText,
                rosterWeaponPowerText,
                rosterWeaponMagicText,
                rosterArmourText,
                rosterArmourPowerText,
                rosterArmourMagicText,
                rosterNextLvlText;
    public Image rosterPortrait;

    [Header("Inventory Components")]
    public Item selectedItem;
    public Text itemName,
                itemDescription,
                useButtonText;
    public Button useButton,
                  tossButton;
    public ItemSlot[] itemSlots;

    [Header("Use Item Components")]
    public GameObject itemCharacterChoiceMenu;
    public Image[] itemCharacterChoiceSprite;
    public Text[] itemCharacterChoiceNames;
    public Text useOnText;

    [Header("Gold")]
    public Text goldText;

    #region Singleton
	public static PauseMenu instance;

	void Awake()
	{
		// Check to see if there is already an instance of the gameObject in the scene
		if (instance == null)
		{
			// Set the instance to this gameObject
			instance = this;
		}
		else
		{
			// Remove unwanted instances
			Destroy(gameObject);
		}

		// Make sure the gameObject isn't destroyed when a new level is loaded
		DontDestroyOnLoad(gameObject);
	}
	#endregion

	// Use this for initialization
	void Start()
	{
		
	}

	// Update is called once per frame
	void Update()
    {
        // Check to see if the menu should be opened
		if (Input.GetButtonDown("Fire2") && !pauseMenu.activeInHierarchy && !GameManager.instance.gameIsBusy)
		{
            // Open the menu
			OpenMenu();
		}
        else if (Input.GetButtonDown("Fire2") && pauseMenu.activeInHierarchy)
		{
            // Close the menu
			CloseMenu();
		}

        // Update the character stats on the interface
        UpdateMainStats();

        // Update the player's gold
        UpdateGold();

        // Update the party roster buttons on the interface
        UpdateRosterButtons();

        GameManager.instance.menuOpen = menuOpen;
    }

    /// <summary>
    /// Function to update the stats on the main pause menu screen
    /// </summary>
    public void UpdateMainStats()
    {
        // Get the character stats from the game manager
        characterStats = GameManager.instance.characterStats;

        // Loop through the characterStats array
        for (int i = 0; i < characterStats.Length; i++)
        {
            // Check to see if characterStats[i] is visible
            if (characterStats[i].gameObject.activeInHierarchy)
            {
                // Enable the relevant character stats box
                playerStatsBox[i].SetActive(true);

                // Display the character's name
                playerNameText[i].text = characterStats[i].charName;

                // Display the character's level
                playerLvlText[i].text = "Lvl: " + characterStats[i].currentLevel;

                // Display the character's HP
                playerHPText[i].text = "HP: " + characterStats[i].currentHP + "/" + characterStats[i].maximumHP;

                // Display the character's MP
                playerMPText[i].text = "MP: " + characterStats[i].currentMP + "/" + characterStats[i].maximumMP;

                // Set the exp slider's max value
                expSlider[i].maxValue = characterStats[i].expToNextLevel[characterStats[i].currentLevel];

                // Set the slider's current value
                expSlider[i].value = characterStats[i].currentEXP;

                // Display the character's portrait
                playerPortrait[i].sprite = characterStats[i].charSprite;
            }
            else
            {
                // Disable the relevant character stats box
                playerStatsBox[i].SetActive(false);
            }
        }
    }

    /// <summary>
    /// Function to update the player's gold
    /// </summary>
    public void UpdateGold()
    {
        // Update the gold in the UI
        goldText.text = GameManager.instance.currentGold.ToString() + " G";
    }

    /// <summary>
    /// Function to update the party roster buttons
    /// </summary>
    public void UpdateRosterButtons()
    {
        // Loop through the array
        for (int i = 0; i < rosterButtons.Length; i++)
        {
            // Set the same number of roster buttons to be active as there are active characterStats objects
            rosterButtons[i].SetActive(characterStats[i].gameObject.activeInHierarchy);

            // Search any child objects of the status button for text
            rosterButtons[i].GetComponentInChildren<Text>().text = characterStats[i].charName;
        }
    }

    /// <summary>
    /// Function to show the item slots
    /// </summary>
    public void ShowItemSlots()
    {
        // Loop through the array
        for (int i = 0; i < itemSlots.Length; i++)
        {
            // Set the index of each button
            itemSlots[i].buttonIndex = i;

            // Set all buttons to be invisible
            itemSlots[i].gameObject.SetActive(false);
        }

        // Loop through the list
        for (int i = 0; i < GameManager.instance.itemInventory.Count; i++)
        {
            // Make all occupied slots visible
            itemSlots[i].gameObject.SetActive(true);

            // Set the sprite of the item in the slot
            itemSlots[i].itemSprite.sprite = GameManager.instance.itemInventory[i].itemSprite;

            // Set the quantity of the item in the slot
            itemSlots[i].quantityText.text = GameManager.instance.itemQuantity[i].ToString();
        }

        // Make the use button un-interactable
        useButton.interactable = false;

        // Make the toss button un-interactable
        tossButton.interactable = false;
    }

    /// <summary>
    /// Function to select an item when clicked
    /// </summary>
    public void SelectItem(Item item)
    {
        // Set the selected item to the item passed in
        selectedItem = item;

        // If the item type is medicine
        if (selectedItem.itemType == ItemType.medicine)
        {
            // Set the use button text to "Use"
            useButtonText.text = "Use";
        }

        // If the item type is weapon/armour
        if (selectedItem.itemType == ItemType.weapon || selectedItem.itemType == ItemType.armour)
        {
            // Set the use button text to "Equip"
            useButtonText.text = "Equip";
        }

        // Show the name of the item in the interface
        itemName.text = selectedItem.itemName;

        // Show the description of the item in the interface
        itemDescription.text = selectedItem.itemDescription;

        useButton.interactable = true;
        tossButton.interactable = true;
        tossButton.GetComponent<RemoveItemButton>().itemToRemove = selectedItem;
    }

    /// <summary>
    /// Function to open the menu
    /// </summary>
    public void OpenMenu()
    {
        // Make menuOpen true
        menuOpen = true;

		// Show the pause menu
		pauseMenu.SetActive(menuOpen);

        SetRosterText(0);

        // Play the sound for opening up the menu
        AudioManager.instance.PlayeSFX("UI_Beep2");
    }

    /// <summary>
    /// Function to close the menu
    /// </summary>
    public void CloseMenu()
    {
        // Loop through the array
        for (int i = 0; i < windows.Length; i++)
        {
            // Set all windows to be invisible
            windows[i].SetActive(false);
        }

        // Make menuOpen false
        menuOpen = false;
        
        // Hide the pause menu
        pauseMenu.SetActive(menuOpen);

        // Play the sound for closing the menu
        AudioManager.instance.PlayeSFX("UI_Beep2");
    }

    /// <summary>
    /// Function to toggle the window visibility
    /// </summary>
    /// <param name="windowIndex">The index number of the window array</param>
    public void ToggleMenuWindow(int windowIndex)
    {
        // Loop through the array
        for (int i = 0; i < windows.Length; i++)
        {
            // Compare i to windowIndex
            if (i == windowIndex)
            {
                // Set the window to be the opposite of its current visibility
                windows[i].SetActive(!windows[i].activeInHierarchy);

                // Play the button sound
                AudioManager.instance.PlayeSFX("UI_Beep");
            }
            else
            {
                // Set all other windows to be invisible
                windows[i].SetActive(false);
            }
        }

        // Make the item use menu invisible
        itemCharacterChoiceMenu.SetActive(false);
    }

    ///<summary>
    /// Function to open the use item screen
    ///</summary>
    public void OpenItemUse()
    {
        // Make the use item menu invisible
        itemCharacterChoiceMenu.SetActive(true);

        // Play the button sound
        AudioManager.instance.PlayeSFX("UI_Beep");

        // Set the useOnText based on what kind of item has been selected
        useOnText.text = (selectedItem.itemType == ItemType.medicine) ? "Use " + selectedItem.itemName + " on:" : "Equip " + selectedItem.itemName + " to:";

        // Loop through the array
        for (int i = 0; i < itemCharacterChoiceNames.Length; i++)
        {
            // Set the name of each player in the party
            itemCharacterChoiceNames[i].text = GameManager.instance.characterStats[i].charName;

            // Set the portrait of each player in the party
            itemCharacterChoiceSprite[i].sprite = GameManager.instance.characterStats[i].charSprite;

            // Set the visibility of each character button
            itemCharacterChoiceNames[i].transform.parent.gameObject.SetActive(GameManager.instance.characterStats[i].gameObject.activeInHierarchy);
        }
    }

    ///<summary>
    /// Function to close the use item screen
    ///</summary>
    public void CloseItemUse()
    {
        // Make the use item menu invisible
        itemCharacterChoiceMenu.SetActive(false);

        // Play the button sound
        AudioManager.instance.PlayeSFX("UI_Beep");
    }

    /// <summary>
    /// Function to use a selected item
    /// </summary>
    public void UseItem(int selectedCharacter)
    {
        // Use the item on the selected character
        selectedItem.UseItem(selectedCharacter);

        // Close the character select menu
        CloseItemUse();

        // Update the roster text
        SetRosterText(selectedCharacter);

        // Play the button sound
        AudioManager.instance.PlayeSFX("UI_Beep");
    }

    /// <summary>
    /// Function to toggle the character visibility in the roster
    /// </summary>
    /// <param name="charIndex">The index number of the character array</param>
    public void SetRosterText(int charIndex)
    {
        // Set the interface elements
        rosterPortrait.sprite = characterStats[charIndex].charSprite;
        rosterHPText.text = characterStats[charIndex].currentHP.ToString() + "/" + characterStats[charIndex].maximumHP.ToString();
        rosterMPText.text = characterStats[charIndex].currentMP.ToString() + "/" + characterStats[charIndex].maximumMP.ToString();
        rosterATKText.text = characterStats[charIndex].pAttack.ToString();
        rosterDEFText.text = characterStats[charIndex].pDefence.ToString();
        rosterMATKText.text = characterStats[charIndex].mAttack.ToString();
        rosterMDEFText.text = characterStats[charIndex].mDefence.ToString();
        rosterSpeedText.text = characterStats[charIndex].speed.ToString();
        rosterWeaponText.text = (characterStats[charIndex].equippedWeapon != null) ? characterStats[charIndex].equippedWeapon.itemName : "None";
        rosterWeaponPowerText.text = characterStats[charIndex].weaponPower.ToString();
        rosterWeaponMagicText.text = characterStats[charIndex].weaponMagic.ToString();
        rosterArmourText.text = (characterStats[charIndex].equippedArmour != null) ? characterStats[charIndex].equippedArmour.itemName : "None";
        rosterArmourPowerText.text = characterStats[charIndex].armourPower.ToString();
        rosterArmourMagicText.text = characterStats[charIndex].armourMagic.ToString();
        rosterNextLvlText.text = (characterStats[charIndex].expToNextLevel[characterStats[charIndex].currentLevel] - 
                                  characterStats[charIndex].expToNextLevel[characterStats[charIndex].currentEXP]).ToString();
    }

    /// <summary>
    /// Function to save the game
    /// </summary>
    public void SaveGame()
    {
        // Save the game
        GameManager.instance.SaveData();

        // Play the button sound
        AudioManager.instance.PlayeSFX("UseItem");
    }
}
