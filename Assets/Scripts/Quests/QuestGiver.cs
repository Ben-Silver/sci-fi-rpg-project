﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestGiver : MonoBehaviour
{
	/* Public Variables */
	public string questName;
	public bool questGiven;
	public bool inRange;
	public Direction activeDirection;
	public string[] message = new string[1];

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		// If the mouse button is clicked, player is in range, quest is yet not given, player is still and player is facing upwards
		if (Input.GetButtonDown("Fire1") && inRange && !questGiven && !PlayerController.instance.playerMoving && PlayerController.instance.direction == activeDirection)
		{
			// Set the message
			message[0] = "New quest registered: " + questName;

			// Give the quest
			QuestManager.instance.RegisterQuest(questName);

			// Set questGiven to true
			questGiven = true;

			// Show the dialogue box
			DialogueManager.instance.ShowDialogue(message, false);
		}
	}

	// When an object enters this object's proximity
	private void OnTriggerEnter2D(Collider2D other)
	{
		// Check to see if the collider is the player
		if (other.gameObject.tag == "Player" && !questGiven)
		{
			// The player is now in range to open the chest
			inRange = true;
		}
	}

	// When an object exits this object's proximity
	private void OnTriggerExit2D(Collider2D other)
	{
		// Check to see if the collider is the player
		if (other.gameObject.tag == "Player" && !questGiven)
		{
			// The player is no-longer in range to open the chest
			inRange = false;
		}
	}
}
