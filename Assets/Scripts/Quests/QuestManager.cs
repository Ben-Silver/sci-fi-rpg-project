﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QuestManager : MonoBehaviour
{
	/* Public Variables */
	public List<Quest> quests;

	#region Singleton
	public static QuestManager instance;

	void Awake()
	{
		// Check to see if there is already an instance of the gameObject in the scene
		if (instance == null)
		{
			// Set the instance to this gameObject
			instance = this;
		}
		else
		{
			// Remove unwanted instances
			Destroy(gameObject);
		}

		// Make sure the gameObject isn't destroyed when a new level is loaded
		DontDestroyOnLoad(gameObject);
	}
	#endregion

	// Use this for initialization
	void Start()
	{
		for (int i = 0; i < 5; i++)
		{
			quests.Add(new Quest("boop", false));
		}
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}

	/// <summary>
	/// Function to mark a quest as complete/incomplete by name
	/// </summary>
	/// <param name="qName">The name of the quest we want to find</param>
	/// <param name="qComplete">The bool value of the quest</param>
	public void SetQuestStatus(string qName, bool qComplete)
	{
		// Make sure quests isn't empty
		if (quests.Count > 0)
		{
			// Loop through the list
			for (int i = 0; i < quests.Count; i++)
			{
				// Compare the name of the quest at i with the string passed in
				if (quests[i].questName == qName)
				{
					// Mark the quest as complete/incomplete
					quests[i].questComplete = qComplete;
				}
			}
		}
	}

	/// <summary>
	/// Function to mark a quest as complete/incomplete by index
	/// </summary>
	/// <param name="qIndex">The index of the quest we want to find</param>
	/// <param name="qComplete">The bool value of the quest</param>
	public void SetQuestStatus(int qIndex, bool qComplete)
	{
		// Mark the quest at index qIndex as complete/incomplete
		quests[qIndex].questComplete = qComplete;
	}

	/// <summary>
	/// Function to get the status of a quest by name
	/// </summary>
	/// <param name="qName">The name of the quest we want to find</param>
	public bool GetQuestStatus(string qName)
	{
		// Make sure quests isn't empty
		if (quests.Count > 0)
		{
			// Loop through the list
			for (int i = 0; i < quests.Count; i++)
			{
				// Compare the name of the quest at i with the string passed in
				if (quests[i].questName == qName)
				{
					// Return the value of the quest status
					return quests[i].questComplete;
				}
			}

			// Return nothing
			return false;
		}
		else
		{
			// Return nothing
			return false;
		}
	}

	/// <summary>
	/// Function to get the status of a quest by index
	/// </summary>
	/// <param name="qIndex">The index of the quest we want to find</param>
	public bool GetQuestStatus(int qIndex)
	{
		// Mark the quest at index qIndex as complete/incomplete
		return quests[qIndex].questComplete;
	}

	/// <summary>
	/// Function to register a new quest
	/// </summary>
	/// <param name="qName">The name of the new quest being registered<param>
	public void RegisterQuest(string qName)
	{
		// Register a brand new quest
		quests.Add(new Quest(qName, false));
	}
}

[System.Serializable]
public class Quest
{
	/* Public Variables */
	public string questName;
	public bool questComplete;

	// Constructor
	public Quest(string qName, bool qComplete)
	{
		// Set questName to the string passed in
		questName = qName;

		// Set questComplete to the bool passed in
		questComplete = qComplete;
	}
}
