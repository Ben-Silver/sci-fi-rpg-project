﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemSlot : MonoBehaviour
{
	/* Public Variables */
	public Image itemSprite;
	public Text quantityText;
	public int buttonIndex;

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}

	/// <summary>
	/// Function for when the itemSlot is selected
	/// </summary>
	public void Press()
	{
		// Select the item in the inventory
		PauseMenu.instance.SelectItem(GameManager.instance.GetItemDetails(GameManager.instance.itemInventory[buttonIndex].itemName));
	}
}
