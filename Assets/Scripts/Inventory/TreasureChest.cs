﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureChest : MonoBehaviour
{
	/* Public Variables */
	public Item itemInChest;
	public bool chestOpened;
	public SpriteRenderer spriteRenderer;
	public Sprite[] chestSprite;
	public bool inRange;
	public Direction activeDirection;
	public string[] message = new string[1];

	// Use this for initialization
	void Start()
	{
		// Find the SpriteRenderer on the gameObject
		spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update()
	{
		// Set the chest sprite based on whether it has been opened or not
		spriteRenderer.sprite = (!chestOpened) ? chestSprite[0] : chestSprite[1];

		// If the mouse button is clicked, player is in range, chest is not opened, player is still and player is facing upwards
		if (Input.GetButtonDown("Fire1") && inRange && !chestOpened && !PlayerController.instance.playerMoving && PlayerController.instance.direction == activeDirection)
		{
			// Set the message
			message[0] = "You found a " + itemInChest.itemName;

			// Add the item
			AddItem(itemInChest);
		}
	}

	// When an object enters this object's proximity
	private void OnTriggerEnter2D(Collider2D other)
	{
		// Check to see if the collider is the player
		if (other.gameObject.tag == "Player" && !chestOpened)
		{
			// The player is now in range to open the chest
			inRange = true;
		}
	}

	// When an object exits this object's proximity
	private void OnTriggerExit2D(Collider2D other)
	{
		// Check to see if the collider is the player
		if (other.gameObject.tag == "Player" && !chestOpened)
		{
			// The player is no-longer in range to open the chest
			inRange = false;
		}
	}

	/// <summary>
	/// Function to add the item in the chest to the inventory
	/// </summary>
	void AddItem(Item itemInChest)
	{
		// Play the audio clip
		GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip);
		
		// If the list isn't empty
		if (GameManager.instance.itemInventory.Count != 0)
		{
			// Loop through the list
			for (int i = 0; i < GameManager.instance.itemInventory.Count; i++)
			{
				// If the inventory doesn't contain the item
				if (!GameManager.instance.itemInventory.Contains(itemInChest))
				{
					// Add the item
					GameManager.instance.itemInventory.Add(itemInChest);

					// Set the quantity at the new index
					GameManager.instance.itemQuantity[GameManager.instance.itemInventory.IndexOf(itemInChest)] += 1;

					// Chest has been opened
					chestOpened = true;

					// Show the dialogue box
					DialogueManager.instance.ShowDialogue(message, false);

					// Return out of the function
					return;
				}

				// If the item is already in the inventory
				if (GameManager.instance.itemInventory[i].itemName == itemInChest.itemName)
				{
					// Increase the item quantity by 1
					GameManager.instance.itemQuantity[i] += 1;

					// Chest has been opened
					chestOpened = true;
					
					// Show the dialogue box
					DialogueManager.instance.ShowDialogue(message, false);

					// Return out of the function
					return;
				}
			}
		}
		else
		{
			// Add the item
			GameManager.instance.itemInventory.Add(itemInChest);

			// Set the quantity at the new index
			GameManager.instance.itemQuantity[GameManager.instance.itemInventory.IndexOf(itemInChest)] += 1;

			// Chest has been opened
			chestOpened = true;
			
			// Show the dialogue box
			DialogueManager.instance.ShowDialogue(message, false);
		}
	}
}
