﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Item")]
[System.Serializable]
public class Item : ScriptableObject
{
	/* Public Variables */
	[Header("Item Types")]
	public ItemType itemType;
	public EffectType effectType;

	[Header("Item Information")]
	public string itemName;
	[TextArea(0,3)] public string itemDescription;
	public Sprite itemSprite;
	public int itemValue;
	public int itemSaleValue;
	public int modifyAmount;
	
	[Header("Weapon Values")]
	public int weaponPower;
	public int weaponMagic;

	[Header("Armour Values")]
	public int armourPower;
	public int armourMagic;
	public int armourSpeed;

	// Use this for initialization
	void Start()
	{

	}
	
	// Update is called once per frame
	void Update()
	{
		
	}

	/// <summary>
	/// Function to use the item
	/// </summary>
	public void UseItem(int characterToUseOn)
	{
		// Get the character at the index characterToUseOn
		CharacterStats selectedCharacter = GameManager.instance.characterStats[characterToUseOn];

		// If the item is medicine
		if (itemType == ItemType.medicine)
		{
			// If the item affects HP
			if (effectType == EffectType.HP)
			{
				// Modify the currentHP by the modifyAmount
				selectedCharacter.currentHP += modifyAmount;

				// If the currentHP exceeds the max HP
				if (selectedCharacter.currentHP > selectedCharacter.maximumHP)
				{
					// Set the current HP to the maximum HP
					selectedCharacter.currentHP = selectedCharacter.maximumHP;
				}
			}

			// If the item affects MP
			if (effectType == EffectType.MP)
			{
				// Modify the currentHP by the modifyAmount
				selectedCharacter.currentMP += modifyAmount;

				// If the currentHP exceeds the max MP
				if (selectedCharacter.currentMP > selectedCharacter.maximumMP)
				{
					// Set the current MP to the maximum MP
					selectedCharacter.currentMP = selectedCharacter.maximumMP;
				}
			}

			// If the item affects pAttack
			if (effectType == EffectType.ATK)
			{
				// Modify the pAttack by the modifyAmount
				selectedCharacter.pAttack += modifyAmount;
			}

			// If the item affects pDefence
			if (effectType == EffectType.DEF)
			{
				// Modify the pDefence by the modifyAmount
				selectedCharacter.pDefence += modifyAmount;
			}

			// If the item affects mAttack
			if (effectType == EffectType.MATK)
			{
				// Modify the mAttack by the modifyAmount
				selectedCharacter.mAttack += modifyAmount;
			}

			// If the item affects mDefence
			if (effectType == EffectType.MDEF)
			{
				// Modify the mDefence by the modifyAmount
				selectedCharacter.mDefence += modifyAmount;
			}

			// If the item affects speed
			if (effectType == EffectType.Speed)
			{
				// Modify the speeed by the modifyAmount
				selectedCharacter.speed += modifyAmount;
			}

			// If the item affects level
			if (effectType == EffectType.Level)
			{
				// Modify the level by the modifyAmount
				selectedCharacter.currentLevel += modifyAmount;
			}
		}

		// If the item is a weapon
		if (itemType == ItemType.weapon)
		{
			// If the player already has an equipped weapon
			if (selectedCharacter.equippedWeapon != null)
			{
				// Add the equipped weapon back into the inventory
				GameManager.instance.AddItem(selectedCharacter.equippedWeapon);
			}

			// Replace the equipped weapon with the new weapon
			selectedCharacter.equippedWeapon = this;

			// Set the character's weapon power
			selectedCharacter.weaponPower = this.weaponPower;

			// Set the character's weapon magic
			selectedCharacter.weaponMagic = this.weaponMagic;
		}

		// If the item is armour
		if (itemType == ItemType.armour)
		{
			// If the player already has an equipped armour
			if (selectedCharacter.equippedArmour != null)
			{
				// Add the equipped armour back into the inventory
				GameManager.instance.AddItem(selectedCharacter.equippedArmour);
			}

			// Replace the equipped armour with the new armour
			selectedCharacter.equippedArmour = this;

			// Set the character's armour power
			selectedCharacter.armourPower = this.armourPower;

			// Set the character's armour magic
			selectedCharacter.armourMagic = this.armourMagic;
		}
	
		// Remove the item from the inventory
		GameManager.instance.RemoveItem(this);
	}

	/// <summary>
	/// Function to buy a selected item
	/// </summary>
	public void BuyItem()
	{
		// Remove the price of the item from the player's current gold
		GameManager.instance.AddGold(-itemValue);

		// Give the player the item they bought
		GameManager.instance.AddItem(this);
	}
}

public enum ItemType
{
	medicine,
	weapon,
	armour,
	battle,
}

public enum EffectType
{
	None,
	HP,
	MP,
	ATK,
	DEF,
	MATK,
	MDEF,
	Speed,
	Level,
}