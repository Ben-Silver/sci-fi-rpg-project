﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveItemButton : MonoBehaviour
{
	/* Public Variables */
	public Item itemToRemove;

	/// <summary>
	/// Function to remove an item when the button is clicked
	/// </summary>
	public void RemoveItem()
	{
		// Remove the item from the inventory
		GameManager.instance.RemoveItem(itemToRemove);
	}
}
